# coding: utf-8
import matplotlib.pyplot as plt
import numpy as np


def parabola(x):
    y = -2.0 * (x - 14.0) ** 2 + 60.0
    return np.maximum(y, 5)


x = np.linspace(0, 30, num=500)

fig = plt.figure()
ax = fig.add_subplot()

ax.plot(x, parabola(x), lw=2)

ax.grid()
ax.set_xlabel("Alter (yr)")
ax.set_ylabel("Eintauchzeit (s)")

ax.set_xlim(0, 30)
ax.set_ylim(bottom=0, top=70)

fig.tight_layout()

fig.savefig("eintauchzeitkurve.png", bbox_inches="tight", dpi=300)

plt.show()
