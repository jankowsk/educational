# Parabolic Function Plot #

This directory contains example `python` code to plot a modified parabolic function.

You can run the provided `python` script like this:

`python plot.py`
