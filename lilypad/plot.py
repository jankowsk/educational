import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

# days = np.linspace(0, 48, num=49)
days = np.arange(49)


def get_size(time):
    return 2 ** time


def get_radius(size):
    return np.sqrt(size / np.pi)


size = get_size(days)
size = size / np.max(size)

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(days, size, lw=2)

ax.axhline(y=0.5, ls="dashed", color="tab:red")

ax.set_ylabel("Size")
ax.set_xlabel("Time (days)")
ax.grid()
ax.set_xlim(0, 48)

ax.set_yscale("log")

fig.savefig("growth.png", bbox_inches="tight", dpi=200)

fig = plt.figure()
ax = fig.add_subplot(111, aspect="equal")

for item in size:
    circ = Circle(
        xy=(0, 0),
        radius=get_radius(item),
        zorder=np.max(days) - item + 3,
        fill=False,
        edgecolor="black",
    )

    ax.add_patch(circ)

ax.grid()
ax.set_xlabel("x")
ax.set_ylabel("y")

ax.set_xlim(-0.75, 0.75)
ax.set_ylim(-0.75, 0.75)

fig.savefig("size.png", bbox_inches="tight", dpi=200)

plt.show()
