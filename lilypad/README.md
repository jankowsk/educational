# Lily Pad Growth Problem #

This directory contains example `python` code to visualise the lily pad exponential growth problem.

You can run the provided `python` script like this:

`python plot.py`
