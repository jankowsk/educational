# Scratch Visual Programming #

This directory contains an example project for the `Scratch` visual programming toolkit, animating a walking cat.

Load the project either online at [https://scratch.mit.edu/](https://scratch.mit.edu/), or download the `Scratch` software and run it there.
