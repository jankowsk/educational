# Pygame Zero Bomberalien Game #

This directory contains an example game written in the Pygame Zero software framework. You control an alien protagonist and can drop bombs that explode after a few seconds.

To run the game, make sure that the `python` modules `pygame` and `pgzero` are installed on your system and then run the game from the source directory like this:

`pgzrun bomberalien.py`
