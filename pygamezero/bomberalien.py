import sys

import pgzrun
import pygame

WIDTH = 1000
HEIGHT = 500
GRAVITY = 0.3
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GROUND = 450

# main character
alien = Actor("alien_right", anchor=("center", "bottom"))
alien.x = 0
alien.y = GROUND
alien.vy = 0

# enemy
enemy = Actor("alien_left", anchor=("center", "bottom"))
enemy.x = WIDTH
enemy.y = GROUND

# bomb
bomb = Actor("bomb_small", anchor=("center", "bottom"))
bomb.x = -30
bomb.y = -30
bomb.vy = 0
bomb.active = False

beep1 = tone.create("A4", 0.2)
beep2 = tone.create("E4", 0.3)
beep3 = tone.create("C4", 0.4)


def jump_up():
    alien.vy -= 40 * GRAVITY


def hide_bomb():
    bomb.x = -30
    bomb.y = -30
    bomb.image = "bomb_small"
    bomb.active = False


def explode_bomb():
    bomb.image = "bomb_exploding_small"
    sounds.explosion.play()

    clock.schedule_unique(hide_bomb, 0.5)


def throw_bomb():
    if not bomb.active:
        bomb.active = True
        bomb.midbottom = alien.midbottom
        bomb.vy = alien.vy

        clock.schedule_unique(explode_bomb, 2.0)


def gravity_update(item):
    item.vy += GRAVITY
    item.y += item.vy

    # item hit ground
    if item.bottom >= GROUND:
        item.bottom = GROUND
        item.vy = 0


def enemy_update(item):
    item.x -= 2.0

    # wrap around
    if item.x < 0:
        item.x = WIDTH


def draw():
    screen.clear()
    screen.fill(WHITE)
    screen.draw.line((0, GROUND), (WIDTH, GROUND), color=BLACK)
    alien.draw()
    enemy.draw()
    bomb.draw()


def update():
    for item in [alien, bomb]:
        gravity_update(item)

    enemy_update(enemy)

    if keyboard.left:
        alien.image = "alien_left"
        alien.left -= 3

    elif keyboard.right:
        alien.left += 3
        alien.image = "alien_right"

    if keyboard.space and alien.bottom >= 450:
        jump_up()

    if keyboard.b:
        throw_bomb()

    # wrap around left and right
    width = WIDTH + alien.width

    if alien.left > width:
        alien.right = 0
    elif alien.right < 0:
        alien.right = width

    # detect collisions with enemies
    if alien.colliderect(enemy):
        sys.exit()


def on_mouse_down(pos):
    if alien.collidepoint(pos):
        set_alien_hurt()
    else:
        beep1.play()
        beep2.play()
        beep3.play()


def set_alien_hurt():
    alien.image = "alien_hurt"
    sounds.eep.play()
    clock.schedule_unique(set_alien_normal, 1.0)


def set_alien_normal():
    alien.image = "alien"
