# Educational Material #

This repository contains educational material to demonstrate programming concepts or solutions to example problems.

The material is primarily targeted towards beginners with little to no programming experience and in particular secondary school pupils to early university students in non-technical subjects. More experienced users might want to look elsewhere for more advanced problems.

Most solutions are implemented in the Python language because of its easily accessible syntax.

## Author ##

The material and software is primarily developed and maintained by Fabian Jankowski. For more information feel free to contact me via: fabian.jankowski at manchester.ac.uk.

## Citation ##

If you make use of the material, please add a link to this repository.
